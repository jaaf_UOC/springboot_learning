# Springboot_learning


## Proyecto
Proyecto realizado para tener una primera toma de conatcto con SpringBoot. Se trata <br>
de una aplicación donde se pueden añadir, modificar y borrar clientes de una lista.<br>

Se incorpora Thymeleaf para realizar la parte web y proporcionar una interfaz amigable <br>
con el usuario, se genera una pagina inicial, el formulario de creación de usuario y <br>
el de listado de estos.

## Alcance
El proyecto es primer contacto con Spring Boot, por lo tanto se genera la estructura <br>
básica de entidades, repositorios y servicios. Para la implementación de la aplicación <br>
se siguen tutoriales y fuentes de terceros.

## Requisitos
Es necesario tener una base de datos activa con Postgres, para modificar la configuración<br>
se debe abrir el fichero "application.properties" dentro de "src/main/resources"<br>

## Configuración
La aplicación consta de la siguiente configuracion de acceso a la base de datos, modificable en el punto anterior.
- Dirección: localhost
- Puerto: 5432
- BBDD: ClienteApp
- Usuario: variable de entorno DB_USER
- Password: variable deentorno DB_PASSWORD

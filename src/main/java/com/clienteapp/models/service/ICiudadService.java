package com.clienteapp.models.service;

import java.util.List;

import com.clienteapp.models.entity.Ciudad;

public interface ICiudadService {

	
	List<Ciudad> listaCiudades();
}

package com.clienteapp.models.repository;

import org.springframework.data.repository.CrudRepository;

import com.clienteapp.models.entity.Ciudad;

public interface CiudadRepository extends CrudRepository<Ciudad, Long> {
	
	

}

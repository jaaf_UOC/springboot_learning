package com.clienteapp;


import com.clienteapp.models.repository.CiudadRepository;
import com.clienteapp.models.entity.Ciudad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;


@SpringBootApplication
public class ClienteappApplication implements CommandLineRunner{

@Autowired
CiudadRepository ciudadRepository;



	public static void main(String[] args) {SpringApplication.run(ClienteappApplication.class, args);}

	
	@Override
	public void run(String... args) throws Exception{
		
		//Add one city in BBDD
		Ciudad ciudad1=new Ciudad("Mérida");
		ciudadRepository.save(ciudad1);
		
		
	}
	
	
}
